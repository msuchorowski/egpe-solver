function generate_grid(g,omega,k,N,N_part)


    x_range = 1.8*sqrt(sqrt( 4 * g * N_part / pi / k  ) )  

    if x_range<1.8*sqrt(2)
      x_range=1.8*sqrt(2)
    end

   

    if g==0
	x_range = 3.5 * sqrt(1/2/k) 
    end

    dx  = x_range*2/(N-1)      # Width of space step (x)
    dx2 = dx*dx
    
    y_range = x_range
    dy  = y_range*2/(N-1)      # Width of space step (x)
    dy2 = dy*dy
    
    # Creating a meshgrid X,Y
    X = collect(range(-x_range, length = N, stop = x_range))
    Y = collect(range(-y_range, length = N, stop = y_range))
    
    XY=[]
    for i in (1:N)
        for j in (1:N)
            push!(XY,(X[i],Y[j]))
        end
    end
    
    return X, x_range, Y, y_range, XY
end;


