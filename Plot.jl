function plot_init_state(init_state,X,x_range,Y,y_range)
  println("Trial state")
  fig, ax = plt.subplots()
  plt.title("Initial state")
  im1 = ax.pcolormesh(X,Y,abs2.(init_state),cmap="jet")
  plt.colorbar(im1,ax=ax)
  ax.set_xlim(-x_range,x_range)
  ax.set_ylim(-y_range,y_range)
 # plt.show()
  plt.close()
  
  println("Trial state")
  fig, ax = plt.subplots()
  plt.title("Initial state")
  im1 = ax.pcolormesh(X,Y,angle.(init_state),cmap="jet")
  plt.colorbar(im1,ax=ax)
  ax.set_xlim(-x_range,x_range)
  ax.set_ylim(-y_range,y_range)
 # plt.show()
  plt.close()
  
  println("Trial state")
  fig, ax = plt.subplots()
  plt.title("Initial state")
  im1 = ax.pcolormesh(X,Y,real.(init_state),cmap="jet")
  plt.colorbar(im1,ax=ax)
  ax.set_xlim(-x_range,x_range)
  ax.set_ylim(-y_range,y_range)
 # plt.show()
  plt.close()
  
  println("Trial state")
  fig, ax = plt.subplots()
  plt.title("Initial state")
  im1 = ax.pcolormesh(X,Y,imag.(init_state),cmap="jet")
  plt.colorbar(im1,ax=ax)
  ax.set_xlim(-x_range,x_range)
  ax.set_ylim(-y_range,y_range)
 # plt.show()
  plt.close()
end




function plot_eigenstates(X,x_range,Y,y_range,XY,u,g,alpha,omega,L,k,N_part,init_state,last_iter,DIFF,CHEMPOT,ENERGY,AAA,N,folder)



      fig, ax = plt.subplots()
      path = mkpath("$folder/N=$N_part/impurity_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k-grid=$N")

      im1 = ax.pcolormesh(X,Y,abs2.(u),cmap="jet")
      plt.colorbar(im1,ax=ax)
      ax.set_xlim(-x_range,x_range)
      ax.set_ylim(-y_range,y_range)
      filename=path*"/den_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.title("Density g=$g α=$alpha Ω=$omega L=$L k=$k")
      plt.savefig(filename)
      plt.close()

      fig, ax = plt.subplots()

      im1 = ax.pcolormesh(X,Y,angle.(u))
      plt.colorbar(im1,ax=ax)
      ax.set_xlim(-x_range,x_range)
      ax.set_ylim(-y_range,y_range)
      filename=path*"/phase_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.title("Phase g=$g α=$alpha Ω=$omega L=$L k=$k")
      plt.savefig(filename)
      plt.close()


      fig, ax = plt.subplots()
      plt.plot(DIFF)
      plt.title("Convergence g=$g α=$alpha Ω=$omega L=$L k=$k")
      plt.yscale("log")
      filename=path*"/conv_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()
  
      fig, ax = plt.subplots()
      plt.plot(CHEMPOT)
      plt.title("Chemical potential g=$g α=$alpha Ω=$omega L=$L k=$k")
      filename=path*"/chempot_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()
  
       fig, ax = plt.subplots()
      plt.plot(ENERGY)
      plt.title("Energy g=$g α=$alpha Ω=$omega L=$L k=$k")
      filename=path*"/energy_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()
  
  
      fig, ax = plt.subplots()
      plt.plot(AAA)
      plt.title("A g=$g α=$alpha Ω=$omega L=$L k=$k")
      filename=path*"/A_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()

  

  
      filename=path*"/u_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k.jld"
      save(filename, "data", u)
      
      a=real(last(AAA))/N_part
      mu=last(CHEMPOT)
      e=last(ENERGY)
      dx2=(X[2]-X[1])^2
      f = sum(abs.(u).^4*dx2)
      a2=real(A2(u,X,Y,N))/N_part
      dev=real(N_part*a2-N_part*N_part*a*a)


                         
      open("$folder/spectrum-N=$N_part-g=$g.txt", "a") do io
         println(io, "$L $N $alpha $omega $k $a $mu $e $f $g $N_part $a2 $dev")
      end;
      

  
  
end


function plot_eigenstates_control(X,x_range,Y,y_range,XY,u,g,alpha,omega,L,k,N_part,init_state,last_iter,DIFF,CHEMPOT,ENERGY,AAA,N,iter,folder)


      fig, ax = plt.subplots()
      path = mkpath("$folder/N=$N_part/impurity_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k-grid=$N/convergence")


      im1 = ax.pcolormesh(X,Y,abs2.(u),cmap="jet")
      plt.colorbar(im1,ax=ax)
      ax.set_xlim(-x_range,x_range)
      ax.set_ylim(-y_range,y_range)
      filename=path*"/den_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k-iter-$iter.png"
      plt.title("Density g=$g α=$alpha Ω=$omega L=$L k=$k iter=$iter")
      plt.savefig(filename)
      plt.close()


      fig, ax = plt.subplots()

      im1 = ax.pcolormesh(X,Y,angle.(u))
      plt.colorbar(im1,ax=ax)
      ax.set_xlim(-x_range,x_range)
      ax.set_ylim(-y_range,y_range)
      filename=path*"/phase_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k-iter-$iter.png"
      plt.title("Phase g=$g α=$alpha Ω=$omega L=$L k=$k iter=$iter")
      plt.savefig(filename)
      plt.close()



      fig, ax = plt.subplots()
      plt.plot(DIFF)
      plt.title("Convergence g=$g α=$alpha Ω=$omega L=$L k=$k")
      plt.yscale("log")
      filename=path*"/conv_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()
  
  

      fig, ax = plt.subplots()
      plt.plot(CHEMPOT)
      plt.title("Chemical potential g=$g α=$alpha Ω=$omega L=$L k=$k")
      filename=path*"/chempot_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()
  
       fig, ax = plt.subplots()
      plt.plot(ENERGY)
      plt.title("Energy g=$g α=$alpha Ω=$omega L=$L k=$k")
      filename=path*"/energy_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()
  
  
      fig, ax = plt.subplots()
      plt.plot(AAA)
      plt.title("A g=$g α=$alpha Ω=$omega L=$L k=$k")
      filename=path*"/A_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k_$k.png"
      plt.savefig(filename)
      plt.close()

  

  
     filename=path*"/u_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k.jld"
     save(filename, "data", u)

  
end

