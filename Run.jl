using Pkg;
Pkg.build("PyCall")
Pkg.add("PyCall")
Pkg.build("PyCall")

using PyPlot
using LinearAlgebra, Statistics, SparseArrays, Random, Arpack
using SpecialPolynomials
using KrylovKit
using JLD
using MAT
using Interpolations, StaticArrays
using Dierckx
using Dates




include("Grid.jl");
include("TrialFunction.jl");
include("Plot.jl");
include("Hamiltonian.jl");
include("SolveGPE.jl");





N0=1000
k0=0.5

g=1.0 #boson-boson interaction coefficient




eps=8 #accuracy of the convergence
N=801 # grid size in one direction



omega=0.25  # omega=2*B_rot where B_rot is rotational constant

dt=0.001 #time step
control_step = 1000 #save control files each "control_step" iteraction


Niter_gs=2^19 #maximal number of the iterations


N_part = 10000 #number of bosons
L=1/N_part # total angular momentum per boson
alpha=0.1   # impurity-bath interaction strength


folder="data_directory" #path to directory to save the data


k = k0*N0 / N_part #harmonic constant of the trp



a1 = solve_GPE_Euler(g,alpha,omega,L,k,N_part,dt,N,eps,Niter_gs,control_step,folder)




