function solve_GPE_Euler(g,alpha,omega,L,k,N_part,dt,N,eps,Niter,control_step,folder)
    ST_DEV=[] 


    X, x_range, Y, y_range, XY  = generate_grid(g,omega,k,N,N_part)
    dx=abs(X[1]-X[2])
    dx2=dx*dx
    #u = trial_u(k,X,Y,N)
    u = load_state(g,alpha,omega,L,k,N)
    u = normalise(u,X,Y)



    u = reshape(u,N^2)
    #build Hamiltonian
    println("g=$g omega=$omega L=$L alpha=$alpha k=$k xrange=$x_range dt=$dt")

  
   
    
    T=t(X,N)
    V=v(alpha,k,XY,N)
    NL=nl(u,g,N,N_part)
    I=iden(dt,N)
    II=iden(1,N)
    Rot,AA = rot_impurity(u,L,omega,XY,X,Y,N,N_part) 
    D=T+V+NL+Rot+I
        
  
    DIFF = []
    CHEMPOT = []
    ENERGY = []
    AAA = []


    
    #solve the eigenproblem and iterate the solution until it converge
    println("Iteration starts - Niter=$Niter")

  
    println("Iter: ")

 
    u_prev = u
    last_iter=Niter
    
    break_control=0

    for it in (1:Niter) #iteration needed only for non-linear equations
        if it%50 == 0 
            print(" ",it)
        end
        
        u,info =  linsolve(D,u_prev/dt)
        u=normalise(u,X,Y)
     

        diff = sum( abs.(abs2.(u)-abs2.(u_prev)))
        append!(DIFF,diff)
    


        if it%control_step==0 || it==2
            u_plot=reshape(u,N,N)
            plot_eigenstates_control(X,x_range,Y,y_range,XY,u_plot,g,alpha,omega,L,k,N_part,u_plot,last_iter,DIFF,CHEMPOT,ENERGY,AAA,N,it,folder)
        end

        if it%500==0 #round(Niter/10)==0 
           println(" Diff:", diff) 
        end
        
        
        if ( diff < 10.0^(-eps) && it>10 ) 
            println("Break control:", it)
            break_control+=1
        else
            break_control=0
        end
        if ( break_control > 15 ) 
            println("Finished after iteration:", it)
            last_iter=it
        end
                
        u_prev = normalise(1.0*u+0.0*u_prev,X,Y) 
        NL = nl(u_prev,g,N,N_part)
        Rot,AA = rot_impurity(u_prev,L,omega,XY,X,Y,N,N_part)
        D = T + V + NL + Rot +I 
        
        append!(AAA,AA)
        mu=real(chempot(u,T,V,NL,Rot,X,Y,x_range,y_range,XY))
        append!(CHEMPOT,mu)
        append!(ENERGY,real(energy(u,mu,AA,g,omega,N_part,X,Y,x_range,y_range,XY,N)))
            
        if it==last_iter
            break
        end


    end
    u=reshape(u_prev,N,N)
    plot_eigenstates(X,x_range,Y,y_range,XY,u,g,alpha,omega,L,k,N_part,u,last_iter,DIFF,CHEMPOT,ENERGY,AAA,N,folder)
    e=ENERGY[last_iter]
    mu=CHEMPOT[last_iter]
    a=AAA[last_iter]
    println("Energy/chemical potential = $mu, $e")
    println("A = $a")
    return X,x_range,Y,y_range,XY,u,g,alpha,omega,L,k,N_part,u,last_iter,DIFF,CHEMPOT,AA,N
    
end





function chempot(u,T,V,NL,Rot,X,Y,x_range,y_range,XY)
    #u should be given as a vector, not a matrix!
    dx=abs(X[1]-X[2])
    dx2=dx*dx
   
    u=reshape(u,N*N)
    H=T+V+NL+Rot
    u=Vector(u)
    uc=transpose(conj(u))
    
    return uc*H*u*dx2
    
end


function energy(u,mu,A,g,omega,N_part,X,Y,x_range,y_range,XY,N)
    #u should be given as a vector, not a matrix!
    dx=abs(X[1]-X[2])
    dx2=dx*dx
    
    u4 = sum(abs.(u).^4*dx2)

    E=mu-0.5*omega*abs2(A)-0.5*g*u4
    
    return E
    
end
