#ind1 -> column number
#ind2 -> row number

#ensure proper boundary condition
function BoundCond(mat,N)
    
    for k in range(0,N-1,step=1) #czyli do N
        #for m in range(max(0,k-2),min(k+2,N-1),step=1) #czyli do k+2
        for m in range(0,N-1,step=1) #czyli do k+2
            mat[m*N+N,1+k*N]=0
            mat[m*N+N-1,1+k*N]=0
            mat[m*N+N,2+k*N]=0

            mat[1+m*N,N+N*k]=0
            mat[2+m*N,N+N*k]=0
            mat[1+m*N,N-1+N*k]=0
        end
    end
    return mat
end




#calculate A
function A(state,X,Y,N)
    state=reshape(state,N,N)
    dx=abs(X[1]-X[2])
    dx2=dx*dx
    integral = 0
    for p in (3:N-2)
        for q in (3:N-2)
            #integral += ( conj(state[p,q]) * ( X[p]/dx/2*( state[p,q+1] - state[p,q-1] ) - Y[q]/dx/2*( state[p+1,q] - state[p-1,q] )  ) ) * dx2
            integral += ( conj(state[p,q]) * ( X[p]/dx/12*( state[p,q-2] + 8*state[p,q+1] - 8*state[p,q-1] - state[p,q+2] ) - Y[q]/dx/12 * (state[p-2,q] + 8*state[p+1,q] - 8*state[p-1,q] - state[p+2,q] ) ) ) *dx2
            
            
           
	end
    end
    return -1im*integral
end


function A2(state,X,Y,N)
    state=reshape(state,N,N)
    state2=copy(state)
    dx=abs(X[1]-X[2])
    dx2=dx*dx
    integral = 0
    for p in (3:N-2)
        for q in (3:N-2)
            #integral += ( conj(state[p,q]) * ( X[p]/dx/2*( state[p,q+1] - state[p,q-1] ) - Y[q]/dx/2*( state[p+1,q] - state[p-1,q] )  ) ) * dx2
            state2[p,q]=( X[p]/dx/12*( state[p,q-2] + 8*state[p,q+1] - 8*state[p,q-1] - state[p,q+2] ) - Y[q]/dx/12 * (state[p-2,q] + 8*state[p+1,q] - 8*state[p-1,q] - state[p+2,q] ) )
 end
end
    for p in (3:N-2)
        for q in (3:N-2)

            integral += ( conj(state[p,q]) * ( X[p]/dx/12*( state2[p,q-2] + 8*state2[p,q+1] - 8*state2[p,q-1] - state2[p,q+2] ) - Y[q]/dx/12 * (state2[p-2,q] + 8*state2[p+1,q] - 8*state2[p-1,q] - state2[p+2,q] ) ) )*dx2
            
            
           
	end
    end
    return (-1im)^2*integral
end


#build the potential matrix 
#  r^2/2
function v(alpha,k,XY,N)

    R=sqrt(2)
    values = (k*first.(XY).^2 + k*last.(XY).^2)/2 + first.(XY)/R*alpha.*exp.( -1/(R^2)*(first.(XY).^2+last.(XY).^2) ) + zeros(ComplexF64,N^2)
    V = sparse(1:N^2,1:N^2, values)


    return V
    
end 

#build the kinetic matrix (laplasjan)
function t(X,N)

    dx=abs(X[1]-X[2])
    dx2=dx*dx
    
    ind1   = vcat( 1:N^2, (2:N^2).-1, (1:(N^2-1)).+1, ((N+1):N^2).-N, (1:(N^2-N)).+N )
    ind2   = vcat( 1:N^2,   2:N^2,       1:(N^2-1),   (N+1):N^2,         1:(N^2-N)   )
    values = vcat( -4*ones(ComplexF64, length(1:N^2)), 
                    1*ones(ComplexF64, length(2:N^2)), 
                    1*ones(ComplexF64, length(1:(N^2-1))), 
                    1*ones(ComplexF64, length((N+1):N^2)), 
                    1*ones(ComplexF64, length(1:(N^2-N))) )
    
    T=sparse(ind1,ind2,values)
         
    # matrix_checker(Tt,T)
    T=BoundCond(T,N)
    return T*-0.5/dx2
    
end

#build the rotation part (A makes it non-linear!)
# -i*omega*dphi - omega/2*A*dphi - omega/2*dphi^2 + omega*L^2/2
function rot_impurity(state,L,omega,XY,X,Y,N,N_part)
    AA =  A(state,X,Y,N)  
    dx=abs(X[1]-X[2])
    dx2=dx*dx
    ind1   = vcat( 1:N^2,   1:N^2-1   ,   2:N^2     ,  1:N^2-N  , 1+N:N^2  ,  1:N^2-N-1 , 1:N^2-N+1 , 1+N-1:N^2, 1+N+1:N^2  )   
    ind2  = vcat(  1:N^2,   2:N^2     ,   1:(N^2-1) ,  1+N:N^2  , 1:N^2-N  ,  1+N+1:N^2 , 1+N-1:N^2 , 1:N^2-N+1, 1:N^2-N-1  )
    B1 = (2im*(-N_part*L+(N_part-1)/N_part*AA))*last.(XY)+first.(XY) #d/dx 
    B2 = (-2im*(-N_part*L+(N_part-1)/N_part*AA))*first.(XY)+last.(XY) #d/dy
    B3 = -first.(XY).^2 #d2/dy2
    B4 = -last.(XY).^2 #d2/dx2
    B5 = 2*first.(XY).*last.(XY) #d/dx*d/dy
    B6 = N_part*L^2 #free coeff
    values = vcat( - 2*B3/dx2 - 2*B4/dx2  .+ B6 ,
                   (B3/dx2+B2/(2*dx))[1:(N^2-1)] ,                       
                   (B3/dx2-B2/(2*dx))[2:N^2]  , 
                   (B4/dx2+B1/(2*dx))[1:N^2-N], 
                   (B4/dx2-B1/(2*dx))[1+N:N^2], 
                   (B5/(4*dx2))[1:N^2-N-1], #/(4*dx2)
                   -(B5/(4*dx2))[1:N^2-N+1],
                   -(B5/(4*dx2))[1+N-1:N^2],
                   (B5/(4*dx2))[1+N+1:N^2]   )   
    

    Rot = sparse(ind2,ind1,values)  #ind1 -> y, ind2 -> x!!!
    Rot=BoundCond(Rot,N)
    Rot=Rot*omega/2
    return Rot,AA
end



#build the rotation part (A makes it non-linear!)
# -i*omega*dphi - omega/2*A*dphi - omega/2*dphi^2 + omega*L^2/2
function rot(omega,XY,X,Y,N)
    dx=abs(X[1]-X[2])
    dx2=dx*dx
    ind1   = vcat(   2:N^2       ,  1:N^2-1   , 1+N:N^2 , 1:N^2-N )   
    ind2  = vcat(   1:(N^2-1)   ,   2:N^2    ,  1:N^2-N , 1+N:N^2) 
    BX = first.(XY) #X
    BY = last.(XY) #Y
    values = vcat( BX[1:(N^2-1)], -BX[2:N^2], -BY[1:N^2-N], BY[1+N:N^2])
    Rot = sparse(ind2,ind1,values)
    Rot=BoundCond(Rot,N)
    return Rot*omega*(-1im)/(2*dx)
end


function iden(dt,N)
    I = sparse(1:N^2,1:N^2,ones(N^2)/dt)
    return I
end

#build non-linear denisty term
function nl(state,g,N,N_part)
    #ind1   = vcat( 1:N)   
    #ind2  = vcat(  1:N)
    ind1   = vcat( 1:N^2)   
    ind2  = vcat(  1:N^2)
    values = reshape(g*(N_part-1)/N_part*abs2.(state),N^2) 
    NL = sparse(ind1,ind2,values)
    return NL
end

