
function load_state(g,alpha,omega,L,k,N)
    
    if isfile("$folder/N=$N_part/impurity_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k-grid=$N/u_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k.jld")
   	    d = load("$folder/N=$N_part/impurity_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k-grid=$N/u_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k.jld")
    	println("Paramters already calculated. Loading converged file as initial state.")
        u = d["data"]

    elseif isfile("$folder/N=$N_part/impurity_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k-grid=$N/convergence/u_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k.jld")
    	d = load("$folder/N=$N_part/impurity_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k-grid=$N/convergence/u_g=$g-alpha=$alpha-omega=$omega-L=$L-N=$N_part-k=$k.jld")
    	println("Loading file from last iteraction as initial state.")
        u = d["data"]
    else
        X, x_range, Y, y_range, XY  = generate_grid(g,omega,k,N,N_part)
        u = trial_u(k,X,Y,N)
        println("New iteration started. Thomas-Fermi profile as initial state.")
    end


    return u
end


#trial wavefunction
function trial_u(k,X,Y,N)
    u = zeros(ComplexF64,N,N)
    mu=sqrt(N_part*g*k/pi)


    for p in (1:N)
        for q in (1:N)
            u[p,q] = ( mu - k*(X[p]^2+Y[q]^2)/2 )
            if real(u[p,q]) < 0 
		u[p,q]=0
            else
		u[p,q]=sqrt(u[p,q])
            end
        end
    end
    u = normalise(u,X,Y)
    return u
end





#normalise wavefunction to integral |psi|^2 dxdy = N
function normalise(f,X,Y)
    dx=abs(X[1]-X[2])
    dx2=dx*dx
    u_sum = sum(abs.(f).^2*dx2)
    f = f/sqrt(u_sum)*sqrt(N_part) 
    return f
end
